<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>A07_VIGIL</title>
</head>

<body style="text-align: center; font-family: Consolas, 'Andale Mono', 'Lucida Console', 'Lucida Sans Typewriter', Monaco, 'Courier New', 'monospace';">
	<div>
		<form action="index.php" method="get">
			<div>
				<label for="firstName">First Name:</label>
				<input type="text" id="firstName" name="first_name" required>
			</div>
			<div>
				<label for="lastName">Last Name:</label>
				<input type="text" id="lastName" name="last_name" required>
			</div>
			<div>
				<label for="username">Username:</label>
				<input type="text" id="username" name="user_name" required><p id="error1" style="color: red;"></p>
			</div>
			<div>
				<label for="Password">Password:</label>
				<input type="password" id="Password" name="user_password" required><p id="error2" style="color: red;"></p>
			</div>
			<div>
				<label for="passwordConfirm">Confirm Password:</label>
				<input type="password" id="passwordConfirm" name="user_password_confirm" required><p id="error3" style="color: red;"></p>
			</div>
			<div>
				<label for="Email">Email:</label>
				<input type="email" id="Email" name="user_email" required><p id="error6" style="color: red;"></p>
			</div>
			<div>
				<label for="emailConfirm">Confirm Email:</label>
				<input type="email" id="emailConfirm" name="user_email_confirm" required><p id="error7" style="color: red";></p>
			</div>
			<div>
				<label for="phoneNumer">Phone Number:</label>
				<input type="text" id="phoneNumber" name="phone_number" required><p id="error4" style="color: red;"></p>
			</div>
			<div>
				<label for="streetAddress">Street Address:</label>
				<input type="text" id="streetAddress" name="street_address" required>
			</div>
			<div>
				<label for="city">City:</label>
				<input type="text" id="city" name="user_city" required>
			</div>
			<div>
				<label for="state">State:</label>
				<input type="text" id="state" name="user_state" required>
			</div>
			<div>
				<label for="zipCode">Zip Code:</label>
				<input type="text" id="zipCode" name="zip_code" required><p id="error5" style="color: red;"></p>
			</div>
			<div>
				<label for="dateBirth">Date of Birth:</label>
				<input type="date" id="dateBirth" name="date_birth" required> 
			</div>
			<div>
				<button type="submit" id="check" onClick="checkFuction()">Submit</button>
			</div>
		</form>
	</div>
	<script>
		function checkFuction(){
			//username var
			var uName = document.getElementById("username").value;
			//password var
			var pWord = document.getElementById("Password").value;
			var upperCaseMatch = /[A-Z]/g;
			var lowerCaseMatch = /[a-z]/g;
			var numberMatch = /[0-9]/g;
			var pWordConfirm = document.getElementById("passwordConfirm").value;
			//phone number var
			var pNumber = document.getElementById("phoneNumber").value;
			var phoneNumberMatch = 
			/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
			//Zip Code var
			var zCode = document.getElementById("zipCode").value;
			var zipCodeMatch = /^[0-9]{5}(?:-[0-9]{4})?$/;
			//email var
			var eMail = document.getElementById("Email").value;
			//email confromation var
			var eMailConfirm =document.getElementById("emailConfirm").value;
			
			
			//username Validation
			if(uName.length <5 || uName.length > 12){
				document.getElementById("error1").innerHTML = "Please enter a username with 5 to 12 characters";
			}
			else {
				document.getElementById("error1").innerHTML = "";
			}
			
			//password Validation
			if (pWord.length <5 || pWord.length >12){
				document.getElementById("error2").innerHTML = "Please enter a password with 5 to 12 characters";
			}
			else if(pWord.search(/[0-9]/) == -1){
				document.getElementById("error2").innerHTML = "Please have at least one number";
			}
			else if(pWord.search(/[a-z]/) == -1){
				document.getElementById("error2").innerHTML = "Please have at least one lower case letter";
			}
			else if(pWord.search(/[A-Z]/) == -1){
				document.getElementById("error2").innerHTML = "Please enter at least one upper case letter";
			}
			else if(pWord.search(/[!\%\&]/) == -1){
				document.getElementById("error2").innerHTML = "Please enter at least one !,%, or & for a symbol";
			}
			else {
				document.getElementById("error2").innerHTML = "";
			}
			
			// password confirm validation
			if(pWordConfirm != pWord){
				document.getElementById("error3").innerHTML = "Please type in the same password";
			}
			else {
				document.getElementById("error3").innerHTML = "";
			}
			
			//phone number vvalidation
			if (pNumber.search(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im) == -1){
				document.getElementById("error4").innerHTML = "please enter a vaid phone number";
			}
			else {
				document.getElementById("error4").innerHTML = "";
			}
			//zip code validation
			if (zCode.search(/^\d{5}$|^\d{5}-\d{3}$/) == -1){
				document.getElementById("error5").innerHTML = "please enter a vaid zip code";
			}
			else {
				document.getElementById("error5").innerHTML = "";
			}
			
			//email validation
			if(eMail.search( /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/) == -1){
				document.getElementById("error6").innerHTML = "Please enter a vaid email";
			}
			else {
				document.getElementById("error6").innerHTML = "";
			}
			
			//email conform validation
			if (eMailConfirm != eMail){
				document.getElementById("error7").innerHTML = "Please enter the same email";
			}
			else {
				document.getElementById("error7").innerHTML = "";
			}
		}
	</script>
</body>
</html>